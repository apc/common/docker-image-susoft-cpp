# Base CERN worker image
FROM gitlab-registry.cern.ch/linuxsupport/alma9-base

# Install needed packages
RUN yum update -y \
    && yum install -y \
        cmake3 \
        gcc-toolset-12 \
        gcc-toolset-12-gcc-c++ \
        doxygen \
        git \
        make \
        mlocate \
        nano \
        qt5-qtbase-devel \
        qt5-qtsvg-devel \
        qt5-qttools-devel \
        qt5-qt3d-devel \
        wget \
        dbus-tools \
        bzip2 \
    && yum clean all \
    && updatedb

# Set up DBUS
RUN dbus-uuidgen --ensure=/etc/machine-id

# Sets the locales in UTF8
ENV LC_ALL=C.UTF-8

# Disable Qt graphics 
ENV QT_QPA_PLATFORM=offscreen

# Set the compiler
RUN echo $'#!/bin/sh\nsource scl_source enable gcc-toolset-12 && g++ "$@"\n' > /usr/local/bin/cxx.sh \
    && echo $'#!/bin/sh\nsource scl_source enable gcc-toolset-12 && gcc "$@"\n' > /usr/local/bin/cc.sh \
    && chmod a+x /usr/local/bin/cxx.sh \
    && chmod a+x /usr/local/bin/cc.sh \
    # Tell QMake what compiler to take
    && mkdir /usr/lib64/qt5/mkspecs/linux-g++-devtoolset \
    && echo $'# Config for gcc-toolset-12 \n\
        include(../linux-g++/qmake.conf) \n\
        QMAKE_CC                = /usr/local/bin/cc.sh \n\
        QMAKE_LINK_C            = $$QMAKE_CC \n\
        QMAKE_LINK_C_SHLIB      = $$QMAKE_CC \n\
        QMAKE_CXX               = /usr/local/bin/cxx.sh \n\
        QMAKE_LINK              = $$QMAKE_CXX \n\
        QMAKE_LINK_SHLIB        = $$QMAKE_CXX \n\
        \n' > /usr/lib64/qt5/mkspecs/linux-g++-devtoolset/qmake.conf
ENV CXX=/usr/local/bin/cxx.sh \
    CC=/usr/local/bin/cc.sh \
    CMAKEBIN=cmake3 \
    QMAKESPEC=linux-g++-devtoolset

# Pull repositories
WORKDIR /susoft/ext/
RUN  wget -nv -O /tmp/boost_1_73_0.tar.bz2 https://sourceforge.net/projects/boost/files/boost/1.73.0/boost_1_73_0.tar.bz2/download \
	&& echo "Untar Boost" && tar -xjf /tmp/boost_1_73_0.tar.bz2 -C /susoft/ext && mv /susoft/ext/boost_1_73_0 /susoft/ext/boost && rm -f /susoft/ext/boost_1_73_0.tar.bz2 \
    && git clone --depth 1 --single-branch -b 2016-12-19 https://gitlab.cern.ch/apc/susofts/thirdparty/tut-framework.git tut-framework \
    && git clone --depth 1 --single-branch -b 3.4.0 https://gitlab.cern.ch/apc/susofts/thirdparty/eigen.git eigen

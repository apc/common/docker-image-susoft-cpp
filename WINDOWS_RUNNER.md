Windows CI Worker
=================

In order to have an operational Windows worker, you need to follow these steps:

1. Create a Windows Virtual Machine on CERN's OpenStack
2. Install the requirements
	- Git for Windows
	- Visual Studio 2019
	- Libraries
	- Setup environment
3. Gitlab Runner
	- Install it
	- Configure it
	- Start it

1. Create a Windows Virtual Machine on CERN's OpenStack
-------------------------------------------------------

On <https://http://openstack.cern.ch/>, create a Windows Virtual Machine, with the highest requirements possible. You'll need to wait around 1 day for it to be fully operational.

Once done, uninstall all unnecessary programs from `CMF`.

2. Install the requirements
---------------------------

### Git for Windows ###

Download it from <https://git-scm.com/downloads>.

Install it with the following options (if nothing specified, keep the default):

- Use the Nano editor
- Use Git and optional Unix tools from the Windows Command Prompt
- Checkout Windows-style, commit Unix-style line endings
- Use Windows' default console windows

**Note that this configuration is *NOT* recommended for development.**

### Visual Studio 2019 ###

Install *Visual Studio 2019 Community* from the official website, or from CMF if it exists (and reboot). Then, from the Applications parameters, you need to update it. Once done, modify the installation and keep only these modules:

- Compilers, build tools, and runtimes
	- C++ 2019 Redistributable Update
	- MSVC v142 - VS 2019 C++ x64/86 build tools(v14.22)
- Development activities
	- C++ core features
- SDKs, libraries and frameworks
	- Windows 10 SDK (10.0.16299.0)

### Libraries ###

Install the requirements listed in <https://readthedocs.web.cern.ch/pages/viewpage.action?pageId=22153013>.

- Put all the libraries in a specific folder as described.
- Install all the softwares needed
	- when asked (in CMake installation for instance), use the options to add the softwares in the path for all users

### Setup environment ###

Once finished, clone [SUSoftCMakeCommon](https://gitlab.cern.ch/DataProcessingAndAnalysis/SUSoftCMakeCommon) to a specific location. You need to edit the file `ext_libs.txt` so the variables points to your library locations: you mainly need to change `EXT_LIB_PATH` and `QT_ROOT_PATH` variables.

Finally, you need to add 2 environment variables for the system (not only for the current user):

- `CMAKEBIN`: should have the value `cmake`
- `SUSCMAKEFOLDER`: should have the path that leads to your clone of [SUSoftCMakeCommon](https://gitlab.cern.ch/DataProcessingAndAnalysis/SUSoftCMakeCommon) (e.g. `D:\gitlab-runner\SUSoftCMakeCommon`)

Gitlab Runner
-------------

### Install it ###

Follow these instructions: <https://docs.gitlab.com/runner/install/windows.html>.

To register the runner, refer to this: <https://gitlab.cern.ch/help/ci/runners/README.md#registering-a-specific-runner-with-a-project-registration-token>. Use `https://gitlab.cern.ch/` when asked.

### Configure it ###

In your Gitlab Runner installation folder, edit the file `config.toml` so it looks like the following (change the paths to your setup):

```TOML
concurrent = 1
check_interval = 0

[[runners]]
	name = "Runner for SUSoft projects (EN-SMM-APC)"
	url = "https://gitlab.cern.ch/ci"
	token = "4c67191c5f89b4945ae8eb3450b3c9"
	executor = "shell"
	shell = "cmd"
	builds_dir = "D:/gitlab-runner/builds"
	cache_dir = "D:/gitlab-runner/cache"
	[runners.cache]
```

Note that since [APCSUPPORT-59](https://its.cern.ch/jira/browse/APCSUPPORT-59), we don't need to start the MSVC batch file anymore.

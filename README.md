[![pipeline status](https://gitlab.cern.ch/apc/susofts/shared/sus_ci_cppworker/badges/master/pipeline.svg)](https://gitlab.cern.ch/apc/susofts/shared/sus_ci_cppworker/commits/master)

SUS CI CPPWorker
================

This project has been forked from <https://gitlab.cern.ch/gitlabci-examples/custom_ci_worker>. You can find more info about how to use it in the [Registry page](https://gitlab.cern.ch/apc/susofts/shared/sus_ci_cppworker/container_registry) of this repository.

#### Table of content ####

[Goal](#goal)

[Usage](#usage)

[Automatic build](#automatic-build)

[Windows CI Worker](#windows-ci-worker)

Goal
----

We create a docker image that will be used as a job runner (or worker) for Gitlab CI. Its goal will be to compile and run the tests of the SU projects that can be run on Linux.

The image contains all the prerequisites to build and run SU software (so we don't have to reinstall them at each run, which saves a lot of time):

- a recent C++ compiler (GCC 8)
- CMake
- the different needed libraries
	- Boost
	- Eigen
	- TUT
	- ...

Usage
-----

You can use the container on you local machine. First, you need to install docker. Then:

- build the container with `docker build -t sus_ci_cppworker .`
- run the container: `docker run --rm -it sus_ci_cppworker bash`
	- if you want to start it from *Git for Windows* console, you need to prepend `winpty`:
	- `winpty docker run --rm -it sus_ci_cppworker bash`

Automatic build
---------------

As soon as you commit to this repository, the image will automatically be built thanks to Gitlab-CI. The automatic build may fail for various reasons:

- CERN infrastructure downtime
- network issue
- unavailable resource (repo Git, Boost...)
- ...

If the error looks strange, don't hesitate to rerun the build, it may work after a few tries...

Note that the image must successfully build to be used as a runner for other project's CI.

Windows CI Worker
=================

For more information about the Windows CI worker, have a look on the file [WINDOWS_RUNNER.md](./WINDOWS_RUNNER.md).
